module.exports = function(app) {
  app.get('/example/', function(req, res, next) {
    const result = `Welcome to example<br/>url:${req.path}`
    res.send(result)
    next()
  })
}
