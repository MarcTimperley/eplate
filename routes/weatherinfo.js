const weatherinfo = async(url) => {
  const http = require('http')
  console.log('url:' + JSON.stringify(url))
  const result = {
    'Content-Type': 'text/plain'
  }
  let chunk = ''
  const location = 'London,gb'
  http.get('http://api.openweathermap.org/data/2.5/weather?APPID=f92b689ee55bedadbea824073559ecc8&q=' + location, (res) => {
    res.on('data', data => {
      chunk += data
    })
    res.on('end', () => {
      // console.log(JSON.parse(response))
      try {
        result.body = JSON.parse(chunk).weather[0].description
      //  console.log(result)
      } catch (e) {
        result.body = 'Error converting data'
      }
      console.log(result)
      return result
    })
  })
}

module.exports = weatherinfo
