const parseUrl = (url) => {
  if (!url) return null
  const search = url.substring(url.indexOf('?') + 1)
  const searchParamArray = search.split('&')
  const searchParams = searchParamArray.map(x => ({[x.substring(0, x.indexOf('='))]: x.substring(x.indexOf('=') + 1)}))
  let response = {
    pathname: url.substring(0, url.indexOf('?')),
    pathnameLower: url.substring(0, url.indexOf('?')).toLowerCase(),
    search: search,
    searchParams: searchParams
  }
  return response
}

module.exports = parseUrl
